import streamlit as st

from database.model import Customer, engine
from sqlalchemy.orm import Session
from sqlalchemy import select

st.set_page_config(
    page_title="My first app",
    layout="wide",
    initial_sidebar_state="expanded",
    menu_items={
        "Get help": None,
        "Report a bug": None,
        "About": "alcemy infra homework",
    },
)

def load_data():
  with Session(engine) as session:
    query = select(Customer)
    customers = session.execute(query).scalars().all()
    customer_list = [customer.__dict__ for customer in customers]
    _ = [customer.pop("_sa_instance_state") for customer in customer_list]  # lord please forgive me
    return customer_list


if __name__ == "__main__":
  data = load_data()

  st.table(data)
