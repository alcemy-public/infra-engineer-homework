import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
engine = sa.create_engine('sqlite:///some_database.sqlite')

class Customer(Base):
    __tablename__ = "customer"
    id = sa.Column(sa.Integer(), primary_key=True)
    name = sa.Column(sa.String(255), nullable=False)
    field = sa.Column(sa.String(255), nullable=False)
