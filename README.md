# alcemy Infrastructure Engineer Homework

## Setup

This app uses Streamlit, a web app framework for Python. In order to run it, make sure you have a recent version of Python installed. Then use pip to install pipenv and use that to install this app's dependencies like so: `pipenv install`.

## Running

The app can then be started simply by calling `streamlit run homework/app.py`. This will start the interpreter and open port 8501 which can then be accessed locally.

## Testing

Tests (not yet fully implemented) can be run simply by typing `pytest`.